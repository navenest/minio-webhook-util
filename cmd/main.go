package main

import (
	"context"
	"errors"
	logz "gitlab.com/navenest/golang-library/pkg/logger/log"
	"gitlab.com/navenest/golang-library/pkg/opentelemetry"
	"gitlab.com/navenest/minio-webhook-util/web"
)

func main() {
	logz.Logger.Info("logger construction succeeded")
	ctx := context.Background()

	// Set up OpenTelemetry.
	otelShutdown, err := opentelemetry.SetupOTelSDK(ctx)
	if err != nil {
		return
	}
	// Handle shutdown properly so nothing leaks.
	defer func() {
		err = errors.Join(err, otelShutdown(context.Background()))
	}()
	webserver := web.App{}
	webserver.Initialize()
	webserver.Run(":8080")
}
