package web

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/navenest/golang-library/pkg/logger/log"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin"
	"go.uber.org/zap"
	"net/http"
	"slices"
	"time"
)

type App struct {
	Router *gin.Engine
}

type health struct {
	Status string `json:"status"`
}

func (web *App) loggingMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		ignorePaths := []string{"/healthcheck"}
		if slices.Contains(ignorePaths, c.Request.URL.Path) {
			return
		}
		start := time.Now()
		c.Next()
		latency := time.Since(start)
		log.Logger.Info("web response",
			zap.String("client_ip", c.ClientIP()),
			zap.String("method", c.Request.Method),
			zap.String("path", c.Request.URL.Path),
			zap.Int("status_code", c.Writer.Status()),
			zap.Int("body_size", c.Writer.Size()),
			zap.Duration("latency", latency))
	}
}

func (web *App) Initialize() {
	gin.SetMode(gin.ReleaseMode)
	web.Router = gin.New()
	web.Router.Use(otelgin.Middleware("gin"))
	web.Router.Use(web.loggingMiddleware())
	web.initializeRoutes()
	//p := prometheus.NewPrometheus("echo", nil)
	//p.Use(web.Router)

	//web.Router.Use(middleware.GzipWithConfig(middleware.GzipConfig{
	//	Skipper: func(c echo.Context) bool {
	//		return strings.Contains(c.Path(), "metrics") // Change "metrics" for your own path
	//	},
	//}))
}

func (web *App) Run(addr string) {
	if addr == "" {
		addr = ":8080"
	}
	err := http.ListenAndServe(addr, web.Router)
	if err != nil {
		log.Logger.Fatal("Issues with webserver: "+err.Error(),
			zap.Error(err))
	}
}

func (web *App) initializeRoutes() {
	web.Router.GET("/healthcheck", web.healthcheck)
	web.Router.POST("/object", web.objectWebhookReceiver)
	web.Router.POST("/audit", web.auditWebhookReceiver)
}

//	func (web *App) healthcheck(c echo.Context) error {
//		data := health{
//			Status: "UP",
//		}
//		return c.JSON(http.StatusOK, data)
//	}
func (web *App) objectWebhookReceiver(c *gin.Context) {
	var request interface{}
	err := c.BindJSON(&request)
	if err != nil {
		c.Status(http.StatusInternalServerError)
		log.Logger.Error("Unable to bind json",
			zap.Error(err))
		return
	}
	log.Logger.Info("object message",
		zap.Any("object", request))
	c.Status(http.StatusAccepted)
}

func (web *App) auditWebhookReceiver(c *gin.Context) {
	var request interface{}
	err := c.BindJSON(&request)
	if err != nil {
		c.Status(http.StatusInternalServerError)
		log.Logger.Error("Unable to bind json",
			zap.Error(err))
		return
	}
	log.Logger.Info("audit message",
		zap.Any("audit", request))
	c.Status(http.StatusAccepted)
}

func (web *App) healthcheck(c *gin.Context) {
	data := health{
		Status: "UP",
	}
	c.JSON(http.StatusOK, data)
}
